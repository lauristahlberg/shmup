﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTrail : MonoBehaviour {

    private Rigidbody2D rb;

    public float growthSpeed;

    public float size;

	// Use this for initialization
	void Start () {

        rb = GetComponent<Rigidbody2D>();

        transform.localScale = new Vector3(0f, 0f, 1f);

    }
	
	// Update is called once per frame
	void FixedUpdate () {

        Resize();
        Debug.Log(transform.localScale.x);

	}

    void Resize()
    {
        if (rb.velocity.x > size || rb.velocity.y > size)
        {
            if (transform.localScale.x < 1.46f)
            {
                transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime * growthSpeed,
                    transform.localScale.y, transform.localScale.z);
            }
            if (transform.localScale.y < 1f)
            {
                transform.localScale = new Vector3(transform.localScale.x,
                    transform.localScale.y + Time.deltaTime * growthSpeed, transform.localScale.z);
            }
        }
        else if(rb.velocity.x < size || rb.velocity.y < size)
        {
            if (transform.localScale.x > 0)
            {
                transform.localScale = new Vector3(transform.localScale.x - Time.deltaTime * growthSpeed,
                    transform.localScale.y - Time.deltaTime * growthSpeed, transform.localScale.z - Time.deltaTime * growthSpeed);
            }
        }



    }
}
